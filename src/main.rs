use std::sync::Arc;
use std::{collections::HashMap, ops::SubAssign};

use anyhow::Result;
use rand::prelude::*;
use reqwest::{header, Url};
use serde::{Deserialize, Serialize};
use serde_json::json;
use structopt::StructOpt;
use tokio::{sync::Mutex, time::sleep, time::Duration};
use tracing::{debug, error, info};

#[derive(Debug, StructOpt)]
#[structopt(about = "Smash the Slido upvote button and more")]
struct Opt {
    #[structopt(help = "Event from the URL, like 'fgdmfq3e'")]
    event: String,
    #[structopt(subcommand)]
    cmd: Cmd,
}

#[derive(Debug, StructOpt)]
enum Cmd {
    #[structopt(about = "List questions from the event")]
    List,
    #[structopt(about = "Upvote the given question(s)")]
    Upvote {
        #[structopt(help = "Question ID(s) got from 'list' subcommand")]
        question_ids: Vec<usize>,
        #[structopt(short, long, required(true), help = "Number of votes to be added")]
        votes: usize,
        #[structopt(short, long, help = "Max number of parallel voting jobs")]
        jobs: Option<usize>,
    },
    #[structopt(about = "Post a question")]
    Post {
        text: String,
        #[structopt(short, long, help = "Author name")]
        name: Option<String>,
    },
}

#[tokio::main(flavor = "multi_thread")]
async fn main() -> Result<()> {
    tracing_subscriber::fmt().init();

    let opt = Opt::from_args();

    match opt.cmd {
        Cmd::List => {
            info!("Querying...");
            let questions = get_question_list(opt.event).await?;
            print_questions(&questions).await?;
        }
        Cmd::Post { text, name } => {
            info!("Posting the question...");

            info!("Author name: {}", name.as_deref().unwrap_or("(anonymous)"));
            info!("Question text: {}", text);
            println!("Press enter to confirm and proceed, or Ctrl-C to abort...");
            post(opt.event, name, text).await?;

            info!("Done");
        }
        Cmd::Upvote {
            question_ids,
            votes,
            jobs,
        } => {
            info!("Querying...");
            let questions = get_question_list(opt.event.clone()).await?;
            let mut questions: HashMap<_, _> = questions
                .into_iter()
                .map(|q| (q.event_question_id, q))
                .collect();

            let mut selected_questions = vec![];
            for id in question_ids {
                if let Some(q) = questions.remove(&id) {
                    selected_questions.push(q);
                } else {
                    error!("Specified question ID `{}` does not exist", id);
                    return Ok(());
                }
            }

            print_questions(&selected_questions).await?;

            println!("Press enter to confirm and proceed, or Ctrl-C to abort...");
            let mut _buf: String = String::new();
            std::io::stdin().read_line(&mut _buf)?;

            /*===========================*/
            let mut vote_jobs = vec![];

            // bars
            let multprog = indicatif::MultiProgress::new();
            let mut bars = HashMap::<Question, indicatif::ProgressBar>::new();
            let sty = indicatif::ProgressStyle::default_bar()
                .template("[{elapsed_precise}] {prefix} {pos:>2}/{len:2} {wide_bar:.cyan/blue}")
                .progress_chars("##>.");

            // jobs
            for q in selected_questions.into_iter() {
                vote_jobs.push((q.clone(), votes));

                // bars
                let bar = multprog.add(indicatif::ProgressBar::new(votes as u64));
                bar.set_style(sty.clone());

                bar.set_prefix(format!("{num}", num = q.event_question_id,));
                bars.insert(q.clone(), bar.clone());

                tokio::spawn(async move {
                    loop {
                        bar.tick();
                        sleep(Duration::from_millis(500)).await;
                    }
                });
            }

            let vote_jobs = Arc::new(Mutex::new(vote_jobs));
            let bars = Arc::new(bars);
            let mut tasks = vec![];
            for _ in 0..jobs.unwrap_or(4) {
                let vote_jobs = vote_jobs.clone();
                let event = opt.event.clone();
                let bars = bars.clone();

                let task = tokio::spawn(async move {
                    loop {
                        let mut vote_jobs = vote_jobs.lock().await;
                        let (q, remain) = {
                            let chosen = vote_jobs.choose_mut(&mut thread_rng());
                            if chosen.is_none() {
                                break;
                            }
                            let chosen = chosen.unwrap();
                            chosen.1.sub_assign(1);
                            (chosen.0.clone(), chosen.1)
                        };
                        vote_jobs.retain(|q| q.1 > 0);
                        drop(vote_jobs);

                        let like_res = backoff::future::retry(globalexp(), || async {
                            Ok(upvote(event.clone(), &q).await?)
                        })
                        .await?;

                        if let Some(bar) = bars.get(&q) {
                            if !bar.is_finished() {
                                bar.inc(1);
                                if remain == 0 {
                                    bar.finish();
                                }
                            }
                        }

                        debug!(
                            "Upvoted {} to score = {}",
                            q.event_question_id, like_res.event_question_score
                        );
                    }
                    Result::<()>::Ok(())
                });
                tasks.push(task);
            }
            tasks.push(tokio::task::spawn_blocking(move || {
                multprog.join()?;
                Result::<()>::Ok(())
            }));
            for task in tasks.into_iter() {
                task.await??;
            }
        }
    }

    Ok(())
}

fn globalexp() -> backoff::ExponentialBackoff {
    let mut exp = backoff::ExponentialBackoff::default();
    exp.initial_interval = std::time::Duration::from_secs(15);
    exp.randomization_factor = 0.1;
    exp.multiplier = 1.05;
    exp.max_elapsed_time = Some(std::time::Duration::from_secs(600));
    exp
}

fn exp() -> backoff::ExponentialBackoff {
    let mut exp = backoff::ExponentialBackoff::default();
    exp.initial_interval = std::time::Duration::from_millis(1000);
    exp.multiplier = 2.0;
    exp.max_elapsed_time = Some(std::time::Duration::from_secs(60));
    exp
}

async fn upvote(hash: String, question: &Question) -> Result<LikeResponse> {
    let jar = Arc::new(reqwest::cookie::Jar::default());
    let client = reqwest::Client::builder()
        .cookie_provider(jar.clone())
        .user_agent(fake_useragent::UserAgents::new().random())
        .build()?;

    let url = format!("https://app.sli.do/api/v0.5/app/events?hash={}", hash);
    let event = backoff::future::retry(exp(), || async {
        let event = client
            .get(&url)
            .send()
            .await?
            .json::<EventResponse>()
            .await?;
        Ok(event)
    })
    .await?;

    debug!("events res = {:#?}", event);

    let url = format!(
        "https://app.sli.do/api/v0.5/events/{uuid}/auth",
        uuid = event.uuid
    );
    let auth_res = backoff::future::retry(exp(), || async {
        let auth_res = client
            .post(&url)
            .json(&HashMap::<String, String>::new())
            .send()
            .await?
            .json::<AuthResponse>()
            .await?;
        Ok(auth_res)
    })
    .await?;

    debug!("Auth res = {:#?}", auth_res);

    jar.add_cookie_str(
        &format!(
            "Slido.EventAuthTokens=\"\"{uuid},{access_token}\"\"",
            uuid = event.uuid,
            access_token = auth_res.access_token
        ),
        &"https://app.sli.do".parse::<Url>()?,
    );

    debug!("Cookie jar = {:#?}", jar);

    let url = format!(
        "https://app.sli.do/api/v0.5/events/{uuid}/questions/{qid}/like",
        uuid = event.uuid,
        qid = question.event_question_id
    );
    debug!("POSTing to {}", url);

    tokio::time::sleep(tokio::time::Duration::from_secs(5)).await;
    let like_res = backoff::future::retry(exp(), || async {
        debug!("Trying to like q = {}", question.event_question_id);
        let like_req = client
            .post(&url)
            .json(&json!({"score": 1}))
            .bearer_auth(&auth_res.access_token);
        let like_res = like_req.send().await?.json::<LikeResponse>().await?;
        Ok(like_res)
    })
    .await?;

    debug!("Liked: {:?}", like_res);

    Ok(like_res)
}

async fn post(hash: String, name: Option<String>, text: String) -> Result<PostResponse> {
    let jar = Arc::new(reqwest::cookie::Jar::default());
    let client = reqwest::Client::builder()
        .cookie_provider(jar.clone())
        .user_agent(fake_useragent::UserAgents::new().random())
        .build()?;

    let url = format!("https://app.sli.do/api/v0.5/app/events?hash={}", hash);
    let event = backoff::future::retry(exp(), || async {
        let event = client
            .get(&url)
            .send()
            .await?
            .json::<EventResponse>()
            .await?;
        Ok(event)
    })
    .await?;

    debug!("events res = {:#?}", event);

    let url = format!(
        "https://app.sli.do/api/v0.5/events/{uuid}/auth",
        uuid = event.uuid
    );
    let auth_res = backoff::future::retry(exp(), || async {
        let auth_res = client
            .post(&url)
            .json(&HashMap::<String, String>::new())
            .send()
            .await?
            .json::<AuthResponse>()
            .await?;
        Ok(auth_res)
    })
    .await?;

    debug!("Auth res = {:#?}", auth_res);

    jar.add_cookie_str(
        &format!(
            "Slido.EventAuthTokens=\"\"{uuid},{access_token}\"\"",
            uuid = event.uuid,
            access_token = auth_res.access_token
        ),
        &"https://app.sli.do".parse::<Url>()?,
    );

    debug!("Cookie jar = {:#?}", jar);

    if let Some(name) = &name {
        debug!("Putting the user name");
        let url = format!(
            "https://app.sli.do/api/v0.5/events/{uuid}/user",
            uuid = event.uuid
        );

        let bytes = serde_json::to_vec(&json!({ "name": name }))?;
        let _user = backoff::future::retry(exp(), || async {
            let user_req = client
                .put(&url)
                .header(header::CONTENT_TYPE, "application/json")
                .bearer_auth(&auth_res.access_token)
                .body(bytes.clone());
            debug!("user req = {:#?}", user_req);
            let user = user_req.send().await?.json::<User>().await?;
            Ok(user)
        })
        .await?;
    }

    let url = format!(
        "https://app.sli.do/api/v0.5/events/{uuid}/summary",
        uuid = event.uuid,
    );

    debug!("Getting summary");
    let summary = backoff::future::retry(exp(), || async {
        let summary = client
            .get(&url)
            .bearer_auth(&auth_res.access_token)
            .send()
            .await?
            .json::<SummaryResponse>()
            .await?;
        Ok(summary)
    })
    .await?;

    let url = format!(
        "https://app.sli.do/api/v0.5/events/{uuid}/sections/{section}/statusbar",
        uuid = event.uuid,
        section = summary.by_section.keys().next().unwrap()
    );

    debug!("Getting statusbar");
    let statusbar = backoff::future::retry(exp(), || async {
        let statusbar = client
            .get(&url)
            .bearer_auth(&auth_res.access_token)
            .send()
            .await?
            .json::<StatusbarResponse>()
            .await?;
        Ok(statusbar)
    })
    .await?;

    let url = format!(
        "https://app.sli.do/api/v0.5/events/{uuid}/questions",
        uuid = event.uuid,
    );

    debug!("Posting post");
    let post_res = backoff::future::retry(exp(), || async {
        let post_req = client
            .post(&url)
            .json(&json!({
            "event_id": statusbar.event_id,
            "event_section_id": statusbar.event_section_id,
            "is_anonymous": name.is_none(),
            "path":"/questions",
            "text": text,
            "labels": [],
            "score": 0
            }))
            .bearer_auth(&auth_res.access_token);
        let post_res = post_req.send().await?.json::<PostResponse>().await?;
        Ok(post_res)
    })
    .await?;

    Ok(post_res)
}

async fn get_question_list(hash: String) -> Result<Vec<Question>> {
    let jar = Arc::new(reqwest::cookie::Jar::default());
    let client = reqwest::Client::builder()
        .cookie_provider(jar.clone())
        .build()?;

    let url = format!("https://app.sli.do/api/v0.5/app/events?hash={}", hash);
    let event: EventResponse = client.get(url).send().await?.json().await?;

    debug!("events res = {:#?}", event);

    let url = format!(
        "https://app.sli.do/api/v0.5/events/{uuid}/auth",
        uuid = event.uuid
    );
    let auth_res = client
        .post(url)
        .json(&HashMap::<String, String>::new())
        .send()
        .await?
        .json::<AuthResponse>()
        .await?;

    debug!("Auth res = {:#?}", auth_res);

    jar.add_cookie_str(
        &format!(
            "Slido.EventAuthTokens=\"\"{uuid},{access_token}\"\"",
            uuid = event.uuid,
            access_token = auth_res.access_token
        ),
        &"https://app.sli.do".parse::<Url>()?,
    );

    debug!("Cookie jar = {:#?}", jar);

    let url = format!("https://app.sli.do/api/v0.5/events/{uuid}/questions?path=%2Fquestions&sort=top&highlighted_first=true&limit=1000", uuid = event.uuid);
    let questions: Vec<Question> = client
        .get(url)
        .bearer_auth(&auth_res.access_token)
        .send()
        .await?
        .json()
        .await?;
    Ok(questions)
}

async fn print_questions(questions: &Vec<Question>) -> Result<()> {
    let mut output = "".to_string();
    for question in questions.iter() {
        let Question {
            author: Author { name },
            event_question_id,
            score,
            text,
        } = question;

        output += &format!("ID: {}\n", event_question_id);
        output += &format!("  Likes: {}\n", score);
        let name_opt = textwrap::Options::with_termwidth()
            .initial_indent("")
            .subsequent_indent("        ");
        output += &format!("  Name: {}\n", textwrap::fill(name, name_opt));
        let text_opt = textwrap::Options::with_termwidth()
            .initial_indent("")
            .subsequent_indent("        ");
        output += &format!("  Text: {}\n\n", textwrap::fill(text.trim(), text_opt));
    }

    let mut pager = minus::Pager::new();
    pager.set_prompt("Scroll: PgUp,PgDn; Search: /; Continue: q");
    pager.set_text(output);
    minus::page_all(pager)?;

    Ok(())
}

#[derive(Debug, Serialize, Deserialize)]
struct AuthResponse {
    access_token: String,
    event_id: usize,
    event_user_id: usize,
}

/// There are a lot of fields missing but only these are necessary
#[derive(Debug, Serialize, Deserialize)]
struct EventResponse {
    uuid: String,
    name: String,
}

/// There are a lot of fields missing but only these are necessary
#[derive(Debug, Serialize, Deserialize, Hash, Eq, PartialEq, Clone)]
struct Question {
    author: Author,
    event_question_id: usize,
    score: usize,
    text: String,
}

/// There are a lot of fields missing but only these are necessary
#[derive(Debug, Serialize, Deserialize, Hash, Eq, PartialEq, Clone)]
struct Author {
    #[serde(default = "anonymous")]
    name: String,
}

fn anonymous() -> String {
    "Anonymous".to_string()
}

#[derive(Debug, Serialize, Deserialize)]
struct LikeResponse {
    event_question_id: usize,
    event_question_score: usize,
    event_question_user_score: usize,
}

#[derive(Debug, Serialize, Deserialize)]
struct SummaryResponse {
    #[serde(rename = "bySection")]
    by_section: HashMap<String, Section>,
}

#[derive(Debug, Serialize, Deserialize)]
struct Section {
    questions: usize,
}

#[derive(Debug, Serialize, Deserialize)]
struct StatusbarResponse {
    event_id: usize,
    event_section_id: usize,
    event_section_uuid: String,
}

#[derive(Debug, Serialize, Deserialize)]
struct PostResponse {
    date_created: String,
}

#[derive(Debug, Serialize, Deserialize)]
struct User {
    user_hash: String,
    name: String,
}
